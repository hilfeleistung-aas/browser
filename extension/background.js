const booking = {
  outwardRide: null,
  returnRide: null
};

let travelChains = {};

const Background = {
  receiveMessage: function (msg, sender, sendResponse) {
    if (msg && msg.type && Background.hasOwnProperty(`on${msg.type}`)) {
      return Background[`on${msg.type}`](msg.payload, sender, sendResponse);
    }
  },

  onPageChanged: function (payload) {
    if (payload.page === 'outwardRide') {
      booking['outwardRide'] = null;
      booking['returnRide'] = null;
    } else {
      booking['returnRide'] = null;
    }
    travelChains = {};
  },

  onTravelChainDiscovered: function (payload) {
    travelChains[payload.id] = payload.travelChain;
  },

  onAssistanceRequestedForTravelChain: function (payload) {
    const travelChain = travelChains[payload.id] || null;
    booking[payload.page] = travelChain;
  },

  onCurrentBookingRequested: function (_payload, _sender, sendResponse) {
    sendResponse(booking)
  },

  onDeleteCurrentBookingRequested: function () {
    booking.outwardRide = null;
    booking.returnRide = null;
  }
};

chrome.runtime.onMessage.addListener(Background.receiveMessage);