(function () {
  function parsePlannedSingleRide(dayStr, text) {
    const tpl = document.createElement('template')
    tpl.innerHTML = text.trim();

    const res = tpl.content.children[0];

    const firsts = Array.from(res.getElementsByClassName('first'));
    const lasts = Array.from(res.getElementsByClassName('last'));

    const [day, month, year] = dayStr.split('.');

    dateWithTimeZone = (timeZone, year, month, day, hour, minute, second) => {
      let date = new Date(Date.UTC(year, month, day, hour, minute, second));

      let utcDate = new Date(date.toLocaleString('en-US', { timeZone: "UTC" }));
      let tzDate = new Date(date.toLocaleString('en-US', { timeZone: timeZone }));
      let offset = utcDate.getTime() - tzDate.getTime();

      date.setTime(date.getTime() + offset);

      return date;
    };

    const makeDeparture = (first) => {
      const departureTrainStation = first.querySelector('td.station') ? first.querySelector('td.station').textContent.trim() : ''
      const departureTrack = first.querySelector('td.platform') ? first.querySelector('td.platform').textContent.trim() : ''
      const departureTrainNumber = first.querySelectorAll('td.products a')[0] ? first.querySelectorAll('td.products a')[0].textContent.trim() : ''
      const departureTime = first.querySelector('td.time') ? first.querySelector('td.time').textContent.trim().slice(3, 8) : ''
      const [dpHours, dpMinutes] = departureTime.split(':')
      const departureOn = dateWithTimeZone("Europe/Berlin", `20${year}`, month, day, dpHours, dpMinutes, 0)

      return { departureOn, departureTrainStation, departureTrack, departureTrainNumber }
    }

    const makeTransfer = (last, first) => {
      const transferArrivalTrack = last.querySelector('td.platform') ? last.querySelector('td.platform').textContent.trim() : ''
      const arrivalTime = last.querySelector('td.time') ? last.querySelector('td.time').textContent.trim().slice(3, 8) : ''
      const [arrHours, arrMinutes] = arrivalTime.split(':')
      const transferArrivalOn = dateWithTimeZone("Europe/Berlin", `20${year}`, month, day, arrHours, arrMinutes, 0)
      const transferTrainStation = first.querySelector('td.station') ? first.querySelector('td.station').textContent.trim() : ''
      const transferDepartureTrack = first.querySelector('td.platform') ? first.querySelector('td.platform').textContent.trim() : ''
      const transferDepartureTrainNumber = first.querySelectorAll('td.products a')[0] ? first.querySelectorAll('td.products a')[0].textContent.trim() : ''
      const departureTime = first.querySelector('td.time') ? first.querySelector('td.time').textContent.trim().slice(3, 8) : ''
      const [dpHours, dpMinutes] = departureTime.split(':')
      const transferDepartureOn = dateWithTimeZone("Europe/Berlin", `20${year}`, month, day, dpHours, dpMinutes, 0)

      return { transferTrainStation, transferArrivalOn, transferArrivalTrack, transferDepartureOn, transferDepartureTrack, transferDepartureTrainNumber }
    }

    const makeArrival = (last) => {
      const arrivalTrainStation = last.querySelector('td.station') ? last.querySelector('td.station').textContent.trim() : ''
      const arrivalTrack = last.querySelector('td.platform') ? last.querySelector('td.platform').textContent.trim() : ''
      const arrivalTime = last.querySelector('td.time') ? last.querySelector('td.time').textContent.trim().slice(3, 8) : ''
      const [arrHours, arrMinutes] = arrivalTime.split(':')
      const arrivalOn = dateWithTimeZone("Europe/Berlin", `20${year}`, month, day, arrHours, arrMinutes, 0)

      return { arrivalTrainStation, arrivalOn, arrivalTrack }
    }

    const adjustTime = (prev, next) => {
      if (prev.getTime() > next.getTime()) {
        next.setDate(next.getDate() + 1);
        return adjustTime(prev, next);
      } else {
        return next;
      }
    }

    const plannedDeparture = makeDeparture(firsts.shift());
    const plannedArrival = makeArrival(lasts.pop());
    const plannedTransfers = [];

    for (let i = 0; i < lasts.length; i++) {
      const last = lasts[i];
      const first = firsts[i];

      plannedTransfers.push(makeTransfer(last, first));
    }

    let currentDate = plannedDeparture.departureOn;
    plannedDeparture.departureOn = currentDate.toISOString();
    plannedTransfers.forEach(transfer => {
      currentDate = adjustTime(currentDate, transfer.transferArrivalOn);
      transfer.transferArrivalOn = currentDate.toISOString();
      currentDate = adjustTime(currentDate, transfer.transferDepartureOn);
      transfer.transferDepartureOn = currentDate.toISOString();
    });
    plannedArrival.arrivalOn = adjustTime(currentDate, plannedArrival.arrivalOn).toISOString();

    return { plannedDeparture, plannedTransfers, plannedArrival };
  }

  function makeCheckbox(resultId) {
    const input = document.createElement('input');
    input.id = `check-request-assistance-${resultId}`;
    input.type = 'checkbox';
    input.checked = true;
    input.value = '1';

    const label = document.createElement('label');
    label.for = input.id;
    label.innerText = 'Hilfeleistung anfragen';
    label.onclick = () => {
      input.checked = !input.checked;
    }

    const wrapper = document.createElement('span');
    wrapper.className = 'hase checkbox-wrapper';
    wrapper.appendChild(input);
    wrapper.appendChild(label);

    return [wrapper, input];
  }

  function extractResultId(str) {
    const firstCloseBracket = str.indexOf(']')
    const firstEqualSign = str.indexOf('=')

    return str.slice(firstEqualSign + 1, firstCloseBracket)
  }

  async function fetchTravelChain(day, href, resultId) {
    const hrefWithAdditionalParams = `${href}HWAI=CONNECTION$${resultId}!id=${resultId}!HwaiConId=${resultId}!HwaiDetailStatus=details!&ajax=1`;

    const res = await window.fetch(hrefWithAdditionalParams);
    const text = await res.text();

    return parsePlannedSingleRide(day, text);
  }

  const heading = document.getElementById('tp_overview_headline');
  let page = null;
  let day = null;

  if (heading && heading.innerText.includes("Hinfahrt")) {
    page = "outwardRide";
    day = heading.innerText.slice(-8);
  } else if (heading && heading.innerText.includes("Rückfahrt")) {
    page = "returnRide";
    day = heading.innerText.slice(-8);
  }

  if (!page || !day) {
    return;
  }

  chrome.runtime.sendMessage({
    type: 'PageChanged',
    payload: {
      page
    }
  })

  const resultsTable = document.getElementById('resultsOverview')
  if (resultsTable) {
    for (tbody of resultsTable.querySelectorAll('tbody')) {
      const detailsLinkElement = tbody.getElementsByClassName('floatLeft iconLink open')[0]
      const tableButton = tbody.querySelector('.fareStd.button-inside.tablebutton.center .button-border');
      const returnTripButton = tbody.getElementsByClassName('arrowlink block returnJourney')[0];
      const dateDividerText = tbody.querySelector('.dateDividerText');

      if (detailsLinkElement && tableButton && tableButton) {
        const resultId = extractResultId(detailsLinkElement.rel);

        fetchTravelChain(day, detailsLinkElement.href, resultId).then(tc => {
          chrome.runtime.sendMessage({
            type: 'TravelChainDiscovered',
            payload: {
              id: resultId,
              travelChain: tc
            }
          })
        })

        const [wrapper, checkbox] = makeCheckbox();

        tableButton.prepend(wrapper);
        tableButton.onclick = () => {
          if (checkbox.checked) {
            chrome.runtime.sendMessage({
              type: 'AssistanceRequestedForTravelChain',
              payload: {
                id: resultId,
                page
              }
            })
          }
        };

        if (returnTripButton) {
          const originalOnClick = returnTripButton.onclick;
          returnTripButton.onclick = () => {
            if (checkbox.checked) {

              chrome.runtime.sendMessage({
                type: 'AssistanceRequestedForTravelChain',
                payload: {
                  id: resultId,
                  page
                }
              })
            }
            if (typeof originalOnClick === "function") {
              originalOnClick();
            }
          };
        }
      } else if (dateDividerText) {
        day = dateDividerText.textContent.slice(-8)
      }
    }
  }
})();