import Vue from 'vue'
import App from './App.vue'
import dayjs from 'dayjs';
import utc from "dayjs/plugin/utc";

Vue.config.productionTip = false

dayjs.extend(utc);

new Vue({
  render: h => h(App),
}).$mount('#app')
